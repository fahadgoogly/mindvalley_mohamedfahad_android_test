package com.cashew;

import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.widget.ImageView;

/**
 * Thread to display the downloaded image into the {@link ImageView}
 * or set the default {@link android.graphics.Color} if the download fails
 */
public class DisplayImage implements Runnable {

    final byte[] bytes;
    final ImageView imageView;
    final int defaultResource;//Default color for the ImageView

    public DisplayImage(byte[] bytes, ImageView imageView, int defaultResource){
        this.bytes = bytes;
        this.imageView = imageView;
        this.defaultResource = defaultResource;
    }

    @Override
    public void run() {
        try {
            if (bytes != null) {
                Bitmap bitmap = ImageUtils.decodeByteArray(bytes, imageView);
                imageView.setImageBitmap(bitmap);
            } else {
                ColorDrawable colorDrawable = new ColorDrawable(defaultResource);
                imageView.setImageDrawable(colorDrawable);
            }
        }catch (Exception e){
            Utils.log(e.getMessage());
        }
    }
}
