package com.cashew;

/**
 * Callback interface for {@link JSONLoader}
 */
public interface JSONLoaderCallback {
    void success(String response);
    void failure(String error);
}
