package com.cashew;

/**
 * {@link JSONLoader} instance which holds the {@link Cashew} singleton instance
 * and {@link JSONLoaderCallback} callback interface to populate the JSON string to the UI class.
 */
public class JSONLoader  {

    final Cashew cashew;
    private String loadUrl;
    private JSONLoaderCallback callback;

    public JSONLoader(Cashew cashew){
        this.cashew = cashew;
    }

    /**
     * Checks whether the JSON string is available in MemoryCache.
     * If it's available, calls {@link JSONLoaderCallback#success(String)}.
     * Else, starts {@link DownloadJSON} thread.
     * @param loadUrl - url to download JSON
     * @param callback - callback interface
     */
    public void load(String loadUrl, JSONLoaderCallback callback){
        try {
            this.loadUrl = loadUrl;
            this.callback = callback;
            byte[] bytes = cashew.memoryCache.get(String.valueOf(loadUrl.hashCode()));
            if (bytes != null) {
                String jsonResponse = Utils.getStringFromBytes(bytes);
                callback.success(jsonResponse);
            } else {
                submitForLoading();
            }
        }catch (Exception e){
            Utils.log(e.getMessage());
            callback.failure(e.getMessage());
        }
    }

    //Queues the downloading of JSON to the thread pool
    private void submitForLoading(){
        cashew.cashewExecutorService.submit(new DownloadJSON(loadUrl, this));
    }

    public JSONLoaderCallback getCallback() {
        return callback;
    }

    public void setCallback(JSONLoaderCallback callback) {
        this.callback = callback;
    }
}
