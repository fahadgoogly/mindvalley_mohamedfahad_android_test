package com.cashew;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Thread to download the image from the given URL.
 * {@link DownloadImage} gets the bytes and converts it into {@link Bitmap}
 * of the height and width of the {@link ImageView}.
 * It also stores the bytes in the {@link MemoryCache}.
 */
public class DownloadImage implements Runnable {

    private String loadUrl;
    private ImageLoader imageLoader;
    private ImageView imageView;

    public DownloadImage(ImageLoader imageLoader, String loadUrl, ImageView imageView) {
        this.loadUrl = loadUrl;
        this.imageLoader = imageLoader;
        this.imageView = imageView;
    }

    @Override
    public void run() {
        try{
            InputStream stream = Utils.getInputStream(loadUrl);
            byte[] imageBytes = Utils.toByteArray(stream);

            //Converts the original bytes to the bytes required to display the Bitmap in the ImageView
            Bitmap bitmap = ImageUtils.decodeByteArray(imageBytes, imageView);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            imageBytes = byteArrayOutputStream.toByteArray();

            imageLoader.cashew.memoryCache.put(String.valueOf(loadUrl.hashCode()), imageBytes);
            imageLoader.cashew.imageMap.remove(imageView);

            //Start DisplayImage thread to display the image in ImageView
            DisplayImage displayImage = new DisplayImage(imageBytes, imageView, imageLoader.getDefaultResource());
            Activity activity = (Activity) imageLoader.cashew.context;
            activity.runOnUiThread(displayImage);

        }catch (Exception e){
            Utils.log(e.getMessage());
            ColorDrawable colorDrawable = new ColorDrawable(imageLoader.getDefaultResource());
            imageView.setImageDrawable(colorDrawable);
        }
    }

}
