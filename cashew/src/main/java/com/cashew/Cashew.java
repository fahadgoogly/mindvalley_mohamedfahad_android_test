package com.cashew;

import android.content.Context;
import android.widget.ImageView;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Singleton for image, json loading and caching<br />
 * Call {@link #with(Context)} to get the Cashew singleton instance
 */
public class Cashew {

    static final String TAG = "Cashew";

    static volatile Cashew singleton = null;

    final Context context;
    final ExecutorService cashewExecutorService;
    final Map<ImageView, String> imageMap;
    final MemoryCache memoryCache;

    public Cashew(Context context) {
        this.context = context;
        this.cashewExecutorService = Executors.newFixedThreadPool(5);
        this.imageMap = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
        this.memoryCache = new MemoryCache();
    }

    /**
     * The singleton {@link Cashew} instance.<br />
     * This function initialized the cache and thread pool with the default values.
     * @param context Activity instance
     * @return Singleton instance
     */
    public static Cashew with(Context context){
        if(context != null) {
            if (singleton == null) {
                synchronized (Cashew.class) {
                    if (singleton == null) {
                        singleton = getInstance(context);
                    }
                }
            }
        }
        return singleton;
    }

    /**
     * Create a new {@link Cashew} instance.
     */
    private static Cashew getInstance(Context context){
        return new Cashew(context);
    }

    /**
     * Create a new {@link ImageLoader} instance passing the {@link Cashew} singleton instance.
     * @return ImageLoader instance
     */
    public ImageLoader toImageBitmap(){
        return new ImageLoader(this);
    }

    /**
     * Create a new {@link JSONLoader} instance passing the {@link Cashew} singleton instance.
     * @return JSONLoader instance
     */
    public JSONLoader toJSONObject() {
        return new JSONLoader(this);
    }

    /**
     * Get the Global {@link MemoryCache} instance to set the maximum limit of the memory cache.
     * @return Global MemoryCache instance
     */
    public MemoryCache getMemoryCache() {
        return memoryCache;
    }
}
