package com.cashew;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Memory Cache with least recentry used eviction policy
 */
public class MemoryCache {

    private Map<String, byte[]> cache = Collections.synchronizedMap(new LinkedHashMap<String, byte[]>(0, 1.5f, true));
    private long memorySize = 0;
    private long memoryLimit = 1000000;//Max limit is configurable

    public MemoryCache(){
        setLimit(Runtime.getRuntime().maxMemory()/4);
    }

    /**
     * Sets the maximum limit for the memory cache
     * @param memoryLimit fdfdfdsf
     */
    public void setLimit(long memoryLimit){
        this.memoryLimit = memoryLimit;
    }

    /**
     * Retrieves the bytes for the given id
     * @param id key for the Map
     * @return byte array for the given id
     */
    public byte[] get(String id){
        try{
            if(!cache.containsKey(id)){
                return null;
            }
            return cache.get(id);
        }catch(NullPointerException e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Stores the bytes for the given id
     * @param id - key for the Map
     * @param bytes - bytes to store
     */
    public void put(String id, byte[] bytes){
        try{
            if(cache.containsKey(id)){
                memorySize -= getSizeInBytes(cache.get(id));
            }
            cache.put(id, bytes);
            memorySize += getSizeInBytes(cache.get(id));
            checkSize();
        }catch(Exception e){
            e.printStackTrace();;
        }
    }

    //Clears the least recently used item from the cache
    private void checkSize(){
        try {
            if (memorySize > memoryLimit) {
                Iterator<Map.Entry<String, byte[]>> iterator = cache.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<String, byte[]> entry = iterator.next();
                    memorySize -= getSizeInBytes(entry.getValue());
                    iterator.remove();
                    if (memorySize <= memoryLimit) {
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private long getSizeInBytes(byte[] bytes){
        try {
            if (bytes == null) {
                return 0;
            }
            return bytes.length;
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public void clear(){
        try{
            cache.clear();
            memorySize = 0;
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
