package com.cashew;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

public class ImageUtils {

    static Bitmap decodeByteArray(byte[] bytes, ImageView imageView){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
        int width = imageView.getWidth();
        int bitmapWidth = options.outWidth;
        int bitmapHeight = options.outHeight;
        int height = width * bitmapHeight / bitmapWidth;
        options.inSampleSize = calculateInSampleSize(bitmapWidth, bitmapHeight, width, height);
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
        return bitmap;
    }

    static int calculateInSampleSize(int bitmapWidth, int bitmapHeight, int reqWidth, int reqHeight) {
        int inSampleSize = 1;
        if (bitmapHeight > reqHeight || bitmapWidth > reqWidth) {
            final int halfHeight = bitmapHeight / 2;
            final int halfWidth = bitmapWidth / 2;

            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
}
