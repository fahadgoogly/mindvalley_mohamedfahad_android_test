package com.cashew;

import java.io.InputStream;

/**
 * Thread to download JSON string from the given URL.
 * {@link DownloadJSON} gets the bytes and returns the string through {@link JSONLoaderCallback}
 */
public class DownloadJSON implements Runnable {

    private String loadUrl;
    private JSONLoader jsonLoader;

    public DownloadJSON(String loadUrl, JSONLoader jsonLoader) {
        this.loadUrl = loadUrl;
        this.jsonLoader = jsonLoader;
    }

    @Override
    public void run() {
        try{
            InputStream stream = Utils.getInputStream(loadUrl);
            byte[] bytes = Utils.toByteArray(stream);
            jsonLoader.cashew.memoryCache.put(String.valueOf(loadUrl.hashCode()), bytes);
            String jsonResponse = Utils.getStringFromBytes(bytes);
            jsonLoader.getCallback().success(jsonResponse);
        }catch (Exception e){
            Utils.log(e.getMessage());
            jsonLoader.getCallback().failure(e.getMessage());
        }
    }
}
