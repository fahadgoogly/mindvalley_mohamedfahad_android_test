package com.cashew;

import android.app.Activity;
import android.widget.ImageView;

/**
 * {@link ImageLoader} instance which holds the {@link Cashew} singleton instance.
 * It has methods to assign a default {@link android.graphics.Color} for the {@link ImageView}
 * and to start the downloading of the image from the URL
 */
public class ImageLoader {

    final Cashew cashew;
    private String loadUrl;
    private int defaultResource;

    public ImageLoader(Cashew cashew) {
        this.cashew = cashew;
    }

    /**
     * Sets the image URL
     * @param loadUrl
     * @return ImageLoader instance
     */
    public ImageLoader load(String loadUrl){
        this.loadUrl = loadUrl;
        return this;
    }

    /**
     * Sets the default color resource for the ImageView
     * @param defaultResource default color to be displayed in ImageView
     * @return ImageLoader instance
     */
    public ImageLoader placeHolder(int defaultResource){
        this.defaultResource = defaultResource;
        return this;
    }

    /**
     * Checks whether the image is available in MemoryCache.
     * If it's available, starts {@link DisplayImage} thread.
     * Else, starts {@link DownloadImage} thread.
     * @param imageView ImageView, for which the image has to be downloaded
     */
    public void into(ImageView imageView){
        try {
            byte[] bytes = cashew.memoryCache.get(String.valueOf(loadUrl.hashCode()));
            if (bytes != null) {
                DisplayImage displayImage = new DisplayImage(bytes, imageView, 0);
                Activity activity = (Activity) cashew.context;
                activity.runOnUiThread(displayImage);
            } else {
                if (cashew.imageMap.get(imageView) == null) {
                    cashew.imageMap.put(imageView, String.valueOf(loadUrl.hashCode()));
                    submitForLoading(imageView);
                }
                DisplayImage displayImage = new DisplayImage(bytes, imageView, defaultResource);
                Activity activity = (Activity) cashew.context;
                activity.runOnUiThread(displayImage);
            }
        }catch (Exception e){
            Utils.log(e.getMessage());
        }
    }

    //Queues the downloading of image to the thread pool
    private void submitForLoading(ImageView imageView){
        cashew.cashewExecutorService.submit(new DownloadImage(this, loadUrl, imageView));
    }

    public int getDefaultResource() {
        return defaultResource;
    }

    public void setDefaultResource(int defaultResource) {
        this.defaultResource = defaultResource;
    }
}
