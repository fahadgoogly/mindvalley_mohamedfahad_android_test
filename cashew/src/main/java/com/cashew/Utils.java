package com.cashew;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class Utils {

    static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 4];
        int n;
        while ((n = input.read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, n);
        }
        return byteArrayOutputStream.toByteArray();
    }

    static String getStringFromBytes(byte[] bytes) throws Exception{
        return new String(bytes, "UTF-8");
    }

    static InputStream getInputStream(String url) throws IOException {
        URL imageUrl = new URL(url);
        HttpURLConnection connection;
        if(url.startsWith("https://")) {
            connection = (HttpsURLConnection) imageUrl.openConnection();
        } else {
            connection = (HttpURLConnection) imageUrl.openConnection();
        }
        connection.setConnectTimeout(30000);
        connection.setReadTimeout(30000);
        connection.setInstanceFollowRedirects(true);
        return connection.getInputStream();
    }

    static void log(String logMessage){
        Log.d(Cashew.TAG, logMessage);
    }
}
