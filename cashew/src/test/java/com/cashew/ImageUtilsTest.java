package com.cashew;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by fahad on 29-08-2016.
 */
public class ImageUtilsTest {

    @Test
    public void testCalculateInSampleSize() throws Exception {
        assertEquals(8, ImageUtils.calculateInSampleSize(2448, 1836, 200, 150), 0);
        assertEquals(4, ImageUtils.calculateInSampleSize(2720, 1532, 341, 192), 0);
        assertEquals(16, ImageUtils.calculateInSampleSize(5472, 3648, 253, 168), 0);
    }
}