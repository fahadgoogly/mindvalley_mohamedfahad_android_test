package com.mindvalley.androidtest.Utilities;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by fahad on 29-08-2016.
 */
public class UtilsTest {

    @Test
    public void testGetImageHeight() throws Exception {
        assertEquals(150, Utils.getImageHeight(200, 2448, 1836), 0);
        assertEquals(192, Utils.getImageHeight(341, 2720, 1532), 0);
        assertEquals(168, Utils.getImageHeight(253, 5472, 3648), 0);
    }
}