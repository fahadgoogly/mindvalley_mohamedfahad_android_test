package com.mindvalley.androidtest.Utilities;

public class Constants {

    public static final String PIN_JSON_URL = "http://pastebin.com/raw/wgkJgazE";

    public static final String PIN_ID = "id";
    public static final String PIN_WIDTH = "width";
    public static final String PIN_HEIGHT = "height";
    public static final String PIN_COLOR = "color";
    public static final String PIN_LIKES = "likes";
    public static final String PIN_LIKED_BY_USER = "liked_by_user";
    public static final String PIN_USER = "user";
    public static final String PIN_USER_ID = "id";
    public static final String PIN_USERNAME = "username";
    public static final String PIN_NAME = "name";
    public static final String PIN_PROFILE_PICTURE = "profile_image";
    public static final String PIN_PROFILE_PICTURE_SMALL = "small";
    public static final String PIN_PROFILE_PICTURE_MEDIUM = "medium";
    public static final String PIN_PROFILE_PICTURE_LARGE = "large";
    public static final String PIN_URLS = "urls";
    public static final String PIN_URL_REGUALR = "regular";
    public static final String PIN_URL_SMALL = "small";
    public static final String PIN_URL_THUMB = "thumb";
}
