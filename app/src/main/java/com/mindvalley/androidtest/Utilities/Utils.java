package com.mindvalley.androidtest.Utilities;

public class Utils {

    public static int getImageHeight(int currentWidth, int bitmapWidth, int bitmapHeight){
        return currentWidth * bitmapHeight / bitmapWidth;
    }
}
