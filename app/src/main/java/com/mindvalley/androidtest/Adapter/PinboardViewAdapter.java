package com.mindvalley.androidtest.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cashew.Cashew;
import com.mindvalley.androidtest.Model.PinModel;
import com.mindvalley.androidtest.R;
import com.mindvalley.androidtest.ShowPinActivity;
import com.mindvalley.androidtest.UserProfileActivity;
import com.mindvalley.androidtest.Utilities.Utils;

import java.util.List;

/**
 * Adapter for PinBoard {@link RecyclerView}
 */
public class PinboardViewAdapter extends RecyclerView.Adapter<PinboardViewAdapter.PinboardViewHolder> {

    private List<PinModel> pins;
    private Context context;

    public PinboardViewAdapter(List<PinModel> pins, Context context) {
        this.pins = pins;
        this.context = context;
    }

    @Override
    public PinboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.pin_item, parent, false);
        final PinboardViewHolder holder = new PinboardViewHolder(view, new PinboardViewHolder.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                int id = v.getId();
                switch (id){
                    case R.id.userProfileLayout:
                        startUserProfileActivity(position);
                        break;
                    case R.id.pinImage:
                        startPinActivity(v, position);
                        break;
                }
            }
        });
        return holder;
    }

    private void startPinActivity(View v, int position){
        Intent intent = new Intent(context, ShowPinActivity.class);
        intent.putExtra(ShowPinActivity.PIN_ITEM, pins.get(position));
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)context, v, "pinImage");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            context.startActivity(intent, optionsCompat.toBundle());
        }else{
            context.startActivity(intent);
        }
    }

    private void startUserProfileActivity(int position){
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(UserProfileActivity.PIN_ITEM, pins.get(position));
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return pins.size();
    }

    @Override
    public void onBindViewHolder(final PinboardViewHolder holder, final int position) {
        try {
            final PinModel pinModel = pins.get(position);
            holder.userName.setText(pinModel.getUser().getUsername());
            holder.likeCountView.setText(pinModel.getLikes() > 999 ? (pinModel.getLikes() / 1000 + "k") : String.valueOf(pinModel.getLikes()));
            if (pinModel.isLikedByUser()) {
                holder.likeView.setImageResource(R.drawable.liked);
                holder.likeCountView.setTextColor(context.getResources().getColor(R.color.md_pink_700));
            } else {
                holder.likeView.setImageResource(R.drawable.like);
                holder.likeCountView.setTextColor(context.getResources().getColor(R.color.md_grey_500));
            }

            holder.profileName.setText(pinModel.getUser().getName());

            holder.pinImage.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (Build.VERSION.SDK_INT >= 16) {
                        holder.pinImage.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }else{
                        holder.pinImage.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                    int imageWidth = pinModel.getWidth();
                    int imageHeight = pinModel.getHeight();

                    int pinWidth = holder.pinImage.getWidth();
                    int pinHeight = Utils.getImageHeight(pinWidth, imageWidth, imageHeight);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(pinWidth, pinHeight);
                    params.width = pinWidth;
                    params.height = pinHeight;
                    holder.pinImage.setLayoutParams(params);
                }
            });

            Cashew.with(context)
                    .toImageBitmap()
                    .load(pinModel.getUrls().getPinSmallUrl())
                    .placeHolder(Color.parseColor(pinModel.getColor()))
                    .into(holder.pinImage);

            Cashew.with(context)
                .toImageBitmap()
                .load(pinModel.getUser().getProfileImageSmall())
                .placeHolder(R.color.md_blue_grey_500)
                .into(holder.userPhoto);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static class PinboardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView pinImage;
        public TextView userName;
        public ImageView likeView;
        public TextView likeCountView;
        public ImageView userPhoto;
        public TextView profileName;
        public LinearLayout userProfileLayout;
        public OnItemClickListener onItemClickListener;

        public PinboardViewHolder(View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            userProfileLayout = (LinearLayout)itemView.findViewById(R.id.userProfileLayout);
            pinImage = (ImageView)itemView.findViewById(R.id.pinImage);
            userName = (TextView)itemView.findViewById(R.id.userName);
            likeView = (ImageView)itemView.findViewById(R.id.likeView);
            likeCountView = (TextView)itemView.findViewById(R.id.likeCountView);
            userPhoto = (ImageView)itemView.findViewById(R.id.userPhoto);
            profileName = (TextView)itemView.findViewById(R.id.profileName);
            this.onItemClickListener = onItemClickListener;
            userProfileLayout.setOnClickListener(this);
            pinImage.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (onItemClickListener != null){
                onItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }

        public interface OnItemClickListener{
            void onItemClick(View v, int position);
        }
    }
}
