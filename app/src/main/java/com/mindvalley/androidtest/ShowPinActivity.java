package com.mindvalley.androidtest;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cashew.Cashew;
import com.mindvalley.androidtest.Model.PinModel;

public class ShowPinActivity extends AppCompatActivity {

    public static final String PIN_ITEM = "showpinactivity_pinitem";

    ImageView pinImage;
    ImageView userPhoto;
    ImageView likeView;
    TextView likeCountView;
    TextView profileName;
    PinModel pin;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }

        pin = getIntent().getParcelableExtra(ShowPinActivity.PIN_ITEM);

        likeView = (ImageView)findViewById(R.id.likeView);
        likeCountView = (TextView)findViewById(R.id.likeCountView);
        userPhoto = (ImageView)findViewById(R.id.userPhoto);
        profileName = (TextView)findViewById(R.id.profileName);
        pinImage = (ImageView)findViewById(R.id.pinImage);
    }

    @Override
    protected void onResume() {
        super.onResume();
        likeCountView.setText(pin.getLikes() > 999 ? (pin.getLikes() / 1000 + "k") : String.valueOf(pin.getLikes()));
        if (pin.isLikedByUser()) {
            likeView.setImageResource(R.drawable.liked);
            likeCountView.setTextColor(getResources().getColor(R.color.md_pink_700));
        } else {
            likeView.setImageResource(R.drawable.like);
            likeCountView.setTextColor(getResources().getColor(R.color.md_grey_500));
        }
        profileName.setText(pin.getUser().getName());

        pinImage.setBackgroundColor(Color.parseColor(pin.getColor()));

        pinImage.post(new Runnable() {
            @Override
            public void run() {
                Cashew.with(ShowPinActivity.this)
                        .toImageBitmap()
                        .load(pin.getUrls().getPinRegularUrl())
                        .placeHolder(Color.parseColor(pin.getColor()))
                        .into(pinImage);

                Cashew.with(ShowPinActivity.this)
                        .toImageBitmap()
                        .load(pin.getUser().getProfileImageSmall())
                        .placeHolder(R.color.md_blue_grey_500)
                        .into(userPhoto);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        int imageWidth = pin.getWidth();
        int imageHeight = pin.getHeight();

        int pinWidth = pinImage.getWidth();
        int pinHeight = pinWidth * imageHeight / imageWidth;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(pinWidth, pinHeight);
        params.width = pinWidth;
        params.height = pinHeight;
        int margin = ShowPinActivity.this.getResources().getDimensionPixelSize(R.dimen.pin_image_margin);
        params.setMargins(margin, margin, margin, margin);
        pinImage.setLayoutParams(params);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)){
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                }else{
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
