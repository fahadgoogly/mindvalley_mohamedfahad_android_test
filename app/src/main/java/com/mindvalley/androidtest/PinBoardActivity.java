package com.mindvalley.androidtest;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.cashew.Cashew;
import com.cashew.JSONLoaderCallback;
import com.mindvalley.androidtest.Adapter.PinboardViewAdapter;
import com.mindvalley.androidtest.Model.PinModel;
import com.mindvalley.androidtest.Model.UrlModel;
import com.mindvalley.androidtest.Model.UserModel;
import com.mindvalley.androidtest.Utilities.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class PinBoardActivity extends AppCompatActivity {

    List<PinModel> pins = new ArrayList<>();
    RecyclerView pinBoard;
    PinboardViewAdapter pinboardViewAdapter;
    SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinboard);

        FloatingActionButton addCategory = (FloatingActionButton)findViewById(R.id.addCategoryFAB);
        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PinBoardActivity.this, "Add Category clicked", Toast.LENGTH_SHORT).show();
                //startActivity();
            }
        });

        swipe = (SwipeRefreshLayout)findViewById(R.id.swipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPins();
                swipe.setRefreshing(true);

            }
        });

        pinBoard = (RecyclerView)findViewById(R.id.pinBoard);
        pinBoard.setHasFixedSize(true);

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        pinBoard.setLayoutManager(staggeredGridLayoutManager);

        pinboardViewAdapter = new PinboardViewAdapter(pins, this);
        pinBoard.setAdapter(pinboardViewAdapter);

        loadPins();
    }

    private void loadPins(){
        Cashew.with(this)
                .toJSONObject()
                .load(Constants.PIN_JSON_URL, new JSONLoaderCallback() {
                    @Override
                    public void success(String response) {
                        populatePinBoard(response);
                    }

                    @Override
                    public void failure(String error) {

                    }
                });
    }

    private void populatePinBoard(String response){
        try {
            if (response != null) {
                JSONArray pinJSONArray = new JSONArray(response);
                int pinCount = pinJSONArray.length();
                for (int index = 0; index < pinCount; index++){
                    JSONObject pinObject = pinJSONArray.getJSONObject(index);
                    String id = pinObject.getString(Constants.PIN_ID);
                    int width = pinObject.getInt(Constants.PIN_WIDTH);
                    int height = pinObject.getInt(Constants.PIN_HEIGHT);
                    String color = pinObject.getString(Constants.PIN_COLOR);
                    int likes = pinObject.getInt(Constants.PIN_LIKES);
                    boolean likedByUser = pinObject.getBoolean(Constants.PIN_LIKED_BY_USER);

                    JSONObject user = pinObject.getJSONObject(Constants.PIN_USER);
                    String userId = user.getString(Constants.PIN_USER_ID);
                    String username = user.getString(Constants.PIN_USERNAME);
                    String name = user.getString(Constants.PIN_NAME);
                    JSONObject profileImage = user.getJSONObject(Constants.PIN_PROFILE_PICTURE);
                    String profileImageSmall = profileImage.getString(Constants.PIN_PROFILE_PICTURE_SMALL);
                    String profileImageMedium = profileImage.getString(Constants.PIN_PROFILE_PICTURE_MEDIUM);
                    String profileImageLarge = profileImage.getString(Constants.PIN_PROFILE_PICTURE_LARGE);

                    JSONObject urls = pinObject.getJSONObject(Constants.PIN_URLS);
                    String pinSmallUrl = urls.getString(Constants.PIN_URL_SMALL);
                    String pinRegularUrl = urls.getString(Constants.PIN_URL_REGUALR);
                    String pinThumbUrl = urls.getString(Constants.PIN_URL_THUMB);

                    UserModel userItem = new UserModel(userId, username, name, profileImageSmall, profileImageMedium, profileImageLarge);
                    UrlModel urlItem = new UrlModel(pinRegularUrl, pinSmallUrl, pinThumbUrl);
                    PinModel pin = new PinModel(id, width, height, color, likes, likedByUser, userItem, urlItem);

                    pins.add(pin);
                }

                pinBoard.post(new Runnable() {
                    @Override
                    public void run() {
                        pinboardViewAdapter.notifyDataSetChanged();
                        if (swipe.isRefreshing()){
                            swipe.setRefreshing(false);
                        }
                    }
                });
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
