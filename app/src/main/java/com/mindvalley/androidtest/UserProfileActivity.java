package com.mindvalley.androidtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.cashew.Cashew;
import com.mindvalley.androidtest.Model.PinModel;

public class UserProfileActivity extends AppCompatActivity {

    public static final String PIN_ITEM = "userprofileactivity_pinitem";

    ImageView userPhoto;
    TextView profileName;
    TextView userName;
    PinModel pin;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userprofile);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }

        pin = getIntent().getParcelableExtra(UserProfileActivity.PIN_ITEM);

        userName = (TextView)findViewById(R.id.userName);
        userPhoto = (ImageView)findViewById(R.id.userPhoto);
        profileName = (TextView)findViewById(R.id.profileName);
    }

    @Override
    protected void onResume() {
        super.onResume();
        profileName.setText(pin.getUser().getName());
        userName.setText(pin.getUser().getUsername());

        userPhoto.post(new Runnable() {
            @Override
            public void run() {
                Cashew.with(UserProfileActivity.this)
                        .toImageBitmap()
                        .load(pin.getUser().getProfileImageLarge())
                        .placeHolder(R.color.md_blue_grey_500)
                        .into(userPhoto);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)){
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                }else{
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
