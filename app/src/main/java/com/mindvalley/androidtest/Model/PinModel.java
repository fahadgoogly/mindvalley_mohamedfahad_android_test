package com.mindvalley.androidtest.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Model class to hold the details of the Pin
 */
public class PinModel implements Parcelable {
    String id;
    int width;
    int height;
    String color;
    int likes;
    boolean likedByUser;
    UserModel user;
    UrlModel urls;

    public PinModel(String id, int width, int height, String color, int likes, boolean likedByUser, UserModel user, UrlModel urls) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.color = color;
        this.likes = likes;
        this.likedByUser = likedByUser;
        this.user = user;
        this.urls = urls;
    }

    protected PinModel(Parcel in) {
        user = in.readParcelable(UserModel.class.getClassLoader());
        urls = in.readParcelable(UrlModel.class.getClassLoader());
        id = in.readString();
        width = in.readInt();
        height = in.readInt();
        color = in.readString();
        likes = in.readInt();
        likedByUser = in.readByte() != 0;
    }

    public static final Creator<PinModel> CREATOR = new Creator<PinModel>() {
        @Override
        public PinModel createFromParcel(Parcel in) {
            return new PinModel(in);
        }

        @Override
        public PinModel[] newArray(int size) {
            return new PinModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public UrlModel getUrls() {
        return urls;
    }

    public void setUrls(UrlModel urls) {
        this.urls = urls;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(user, flags);
        dest.writeParcelable(urls, flags);
        dest.writeString(id);
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeString(color);
        dest.writeInt(likes);
        dest.writeByte((byte) (likedByUser ? 1 : 0));
    }
}