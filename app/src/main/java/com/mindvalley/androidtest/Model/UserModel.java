package com.mindvalley.androidtest.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Model class to hold the user details
 */
public class UserModel implements Parcelable{
    String userId;
    String username;
    String name;
    String profileImageSmall;
    String profileImageMedium;
    String profileImageLarge;

    public UserModel(String userId, String username, String name, String profileImageSmall, String profileImageMedium, String profileImageLarge) {
        this.userId = userId;
        this.username = username;
        this.name = name;
        this.profileImageSmall = profileImageSmall;
        this.profileImageMedium = profileImageMedium;
        this.profileImageLarge = profileImageLarge;
    }


    protected UserModel(Parcel in) {
        userId = in.readString();
        username = in.readString();
        name = in.readString();
        profileImageSmall = in.readString();
        profileImageMedium = in.readString();
        profileImageLarge = in.readString();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImageSmall() {
        return profileImageSmall;
    }

    public void setProfileImageSmall(String profileImageSmall) {
        this.profileImageSmall = profileImageSmall;
    }

    public String getProfileImageMedium() {
        return profileImageMedium;
    }

    public void setProfileImageMedium(String profileImageMedium) {
        this.profileImageMedium = profileImageMedium;
    }

    public String getProfileImageLarge() {
        return profileImageLarge;
    }

    public void setProfileImageLarge(String profileImageLarge) {
        this.profileImageLarge = profileImageLarge;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(username);
        dest.writeString(name);
        dest.writeString(profileImageSmall);
        dest.writeString(profileImageMedium);
        dest.writeString(profileImageLarge);
    }
}
