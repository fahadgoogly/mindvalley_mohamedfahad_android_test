package com.mindvalley.androidtest.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Model class to hold the URL's of the image
 */
public class UrlModel implements Parcelable{
    String pinRegularUrl;
    String pinSmallUrl;
    String pinThumbUrl;

    public UrlModel(String pinRegularUrl, String pinSmallUrl, String pinThumbUrl) {
        this.pinRegularUrl = pinRegularUrl;
        this.pinSmallUrl = pinSmallUrl;
        this.pinThumbUrl = pinThumbUrl;
    }

    protected UrlModel(Parcel in) {
        pinRegularUrl = in.readString();
        pinSmallUrl = in.readString();
        pinThumbUrl = in.readString();
    }

    public static final Creator<UrlModel> CREATOR = new Creator<UrlModel>() {
        @Override
        public UrlModel createFromParcel(Parcel in) {
            return new UrlModel(in);
        }

        @Override
        public UrlModel[] newArray(int size) {
            return new UrlModel[size];
        }
    };

    public String getPinRegularUrl() {
        return pinRegularUrl;
    }

    public void setPinRegularUrl(String pinRegularUrl) {
        this.pinRegularUrl = pinRegularUrl;
    }

    public String getPinSmallUrl() {
        return pinSmallUrl;
    }

    public void setPinSmallUrl(String pinSmallUrl) {
        this.pinSmallUrl = pinSmallUrl;
    }

    public String getPinThumbUrl() {
        return pinThumbUrl;
    }

    public void setPinThumbUrl(String pinThumbUrl) {
        this.pinThumbUrl = pinThumbUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pinRegularUrl);
        dest.writeString(pinSmallUrl);
        dest.writeString(pinThumbUrl);
    }
}
